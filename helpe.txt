https://medium.com/@tharinduit16/react-18-with-webpack-5-project-setup-steps-a93b4e1aaa3b
https://medium.com/@abuduabiodunsulaiman/setup-react-app-with-webpack-ts-and-js-da80cf3b7278
https://blog.devgenius.io/eslint-prettier-typescript-and-react-in-2022-e5021ebca2b1
https://dev.to/shivampawar/setup-react-application-using-typescript-and-webpack-2kn6
https://itnext.io/create-react-typescript-project-with-webpack-and-babel-2431cac8cf5b
https://dev.to/knowankit/setup-eslint-and-prettier-in-react-app-357b

youtube ===>serch [React TypeScript Webpack - Setup From Scratch]
https://www.youtube.com/watch?v=z0rkLRLU6Dw&list=PLC3y8-rFHvwiWPS2RO3BKotLRfgg_8WEo&index=5

https://github.com/gopinav/React-TypeScript-Webpack-Starter

npm install -g serve@latest
npx serve -s build


install webpack and babel packages
npm install -D [package name]


to render png svg, ... we need to add declarations.d.ts file 
and add them declare [module '*.png' and declare module '*.svg']
and add related rules to common file to render them (css, style, images,svgs and ...)


to run app in 
dev mode --->     "start": "webpack serve --config webpack/webpack.config.js --env env=dev --open",
prod mode --->    "build": "webpack --config webpack/webpack.config.js --env env=prod"

we can remove --open flag in dev mode (package.json --->inside script)
and add this flag inside devServer (webpack.dev.js)

we add these two configs to packaske.json 

make 4 file in webpack folder dev,prod,common,config
in config file we need to merge common and Env mode(dev or prod)

add ESLint to find and fix problems in js codes -->eslint.org
yarn add -D eslint

//https://eslint.org/docs/latest/rules/quotes.html
to see the warnings and errors in console terminal
yarn lint


.prettier.js file
// module.exports = {
//     semi: false,
//     trailingComma: 'es5',
//     singleQuote: true,
//     printWidth: 80,
//     tabWidth: 2,
//     endOfLine: 'auto'
// }



