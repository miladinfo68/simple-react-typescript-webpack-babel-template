//merge two dev and prod config file hear.
const { merge } = require("webpack-merge");
const commonConfig = require("./webpack.common.js");

module.exports = (envType) => {
    // console.log("xxxxxxxxxx", envType)
    const { env } = envType;
    const envWebpackConfig = require(`./webpack.${env}.js`);
    const config = merge(commonConfig, envWebpackConfig);
    return config;
}