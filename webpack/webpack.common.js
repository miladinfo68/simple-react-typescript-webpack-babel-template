const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin")

module.exports = {
    entry: path.resolve(__dirname, "..", "src/index.tsx"),
    resolve: {
        extensions: [".tsx", ".ts", ".js"]
    },
    module: {
        rules: [
            // add rule for ts,tsx,jsx files
            {
                test: /\.(ts|js)x?$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: "babel-loader"
                    }
                ]
            },
            // add rule for style or css files
            {
                test: /\.css$/,
                use: ["style-loader", "css-loader"]
            },
            // add rule for image files
            {
                test: /\.(?:ico|gif|png|jpg|jpeg)$/i,
                type: "asset/resource"
            },
            // add rule for svg files
            {
                test: /\.(woff(2)?|eot|ttf|otf|svg)$/i,
                type: "asset/inline"
            }
        ]
    },
    output: {
        filename: "bundle.js",
        path: path.resolve(__dirname, "..", "build"),
    },
    // mode: 'development', //remove it to work in both dev and prod mode
    plugins: [
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, "..", "src/index.html")
        })
    ]
}

