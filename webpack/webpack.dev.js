const webpack = require("webpack");
const ReactRefreshWebpackPlugin = require("@pmmmwh/react-refresh-webpack-plugin");

module.exports = {
    mode: "development",
    devtool: "cheap-module-source-map",
    devServer: {
        hot: true,
        open: true
    },
    plugins: [
        new webpack.DefinePlugin({
            // "process.env.NODE_ENV": JSON.stringify(process.env.NODE_ENV),
            process: "process/browser",
            "process.env.name": JSON.stringify("milad in dev mode")
        }),
        //enable hot reload (fast refresh) in dev mode
        new ReactRefreshWebpackPlugin(),
    ]
}