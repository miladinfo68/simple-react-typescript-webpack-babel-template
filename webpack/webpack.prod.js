const webpack = require("webpack");

module.exports = {
    mode: "production",
    devtool: "source-map",
    plugins: [
        new webpack.DefinePlugin({
            // "process.env.NODE_ENV": JSON.stringify(process.env.NODE_ENV),
            process: "process/browser",
            "process.env.name": JSON.stringify("milad in prod mode")
        })
    ]
}