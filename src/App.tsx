import "./styles.css"
import ImgSrc from "./logo.png"
import svgSrc from "./logo.svg"
import { ClickCounter } from "./components/ClickCounter"

const IMAGE = () => <img src={ImgSrc} style={{ width: "400px", height: "400px" }} alt="this is a react png file" />

const SVG = () => <img src={svgSrc} style={{ width: "400px", height: "400px" }} />
let mm = "77777777";

const App = () => {
    return (
        <>
            <h1>111111111 Create Typescript React App by Babel </h1>
            <h3>
                Hot reload enabled by package ReactRefreshWebpackPlugin , if comment this config after every
                changes page post back and refresh.
            </h3>
            <div>
                <p>Type of Environment : {process.env.NODE_ENV} </p>
                <p> UserName : {process.env.name} </p>
            </div>
            <div>
                <IMAGE />
                <SVG />
            </div>

            <ClickCounter />
        </>
    )
}
export default App
